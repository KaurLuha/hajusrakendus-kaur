<?php

namespace App\Http\Controllers;

use App\Models\Motor;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MotorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Motors = Motor::latest()->paginate(10);
        return Inertia::render('Motor', [
            "motors" => $Motors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return Inertia::render('Kaur');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'power' => 'required',
            'year' => 'required',
            'image' => 'required',
        ]);
   
        $imageArray = $request->file('image');
        $path = $imageArray[0]->store('images', 'public');

        Motor::create([
            'name' => $request->name,
            'description' => $request->description,
            'power' => $request->power,
            'year' => $request->year,
            'image' => $path
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Motor  $Motor
     * @return \Illuminate\Http\Response
     */
    public function show(Motor $Motor)
    {
        return [
            "status" => 1,
            "data" =>$Motor
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Motor  $Motor
     * @return \Illuminate\Http\Response
     */
    public function edit(Motor $Motor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Motor  $Motor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Motor $Motor)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);

        $Motor->update($request->all());

        return [
            "status" => 1,
            "data" => $Motor,
            "msg" => "Motor updated successfully"
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Motor  $Motor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Motor $Motor)
    {
        $Motor->delete();
        return [
            "status" => 1,
            "data" => $Motor,
            "msg" => "Motor deleted successfully"
        ];
    }
}