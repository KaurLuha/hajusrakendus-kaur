<?php

use App\Http\Controllers\MarkerController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\MotorController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\StoreController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('motor', MotorController::class);
Route::get('motor', [MotorController::class, 'index'])->name('motor.index');
Route::post('motor/store', [MotorController::class, 'store'])->name('motor.store');
Route::get('motor/create', [MotorController::class, 'create'])->name('motor.create');

Route::resource('blog', BlogController::class)
    ->only(['index', 'store', 'update', 'destroy'])
    ->middleware(['auth', 'verified']);
Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});
Route::resource('comments', CommentController::class)
    ->middleware(['auth', 'verified']);;

Route::get('/map', function(){

});

Route::get('/weather', function() {
    if (Cache::has('key') === false) {
        $response = Http::get('https://api.openweathermap.org/data/2.5/weather?lat=58.244419&lon=22.500055&appid=622501861af2ca9b980af0d49c0ea8cc&units=metric');
        Cache::put('key', $response->json(), now()->addMinutes(10));
    }
    return Inertia::render('Weather',
    ['weather' => Cache::get('key'),
    ]);
})->name('weather');

Route::get('/kaarel', function() {
    if (Cache::has('key2') === false) {
        $response = Http::get('https://hajus.tak20kallas.itmajakas.ee/api/games');
        Cache::put('key2', $response->json(), now()->addMinutes(5));
    }
    return Inertia::render('Kaarel',
    ['kaarel' => Cache::get('key2'),
    ]);
})->name('kaarel');

Route::get('/kaur', function() {
    if (Cache::has('key3') === false) {
        $response = Http::get('https://hajusrakud.tak20luhaaar.itmajakas.ee/api/motor');
        Cache::put('key3', $response->json(), now()->addMinutes(5));
    }
    return Inertia::render('Kaur',
    ['kaur' => Cache::get('key3'),
    ]);
})->name('kaur');

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::controller(MarkerController::class)->name('map')->group(function(){
Route::get('/map', 'index')->name('index'); //read
Route::post('/add-marker', 'store')->name('store'); //create
Route::put('/update-marker', 'store')->name('update'); //update
Route::put('/delete-marker', 'destroy')->name('delete'); //delete

});

Route::get('/store', [StoreController::class, "index"])->middleware(['auth'])->name("store");
Route::post('/addtocart', [StoreController::class, 'addtocart'])->name('addcart');
Route::get('/cart', [StoreController::class, 'cart'])->name('cart');
Route::post('update-cart', [StoreController::class, 'updatecart'])->name('update.cart');
Route::post('remove-from-cart', [StoreController::class, 'deleteitem'])->name('remove.cart');
Route::post('/subscribe', [StoreController::class, 'subscribe'])->name('subscribe');
Route::post('/success', [store::class, 'success'])->name('success');

Route::get('/products', [ProductsController::class, "index"])->middleware(['auth'])->name('products');
Route::get('/products.add', [ProductsController::class, "create"])->middleware(['auth'])->name('products.add');
Route::post('/products.store', [ProductsController::class, "store"])->middleware(['auth'])->name('products.store');

Route::get('/products/edit/{id}', [ProductsController::class, "edit"])->middleware(['auth'])->name('products.edit');
Route::post('/products/update', [ProductsController::class, "update"])->middleware(['auth'])->name('products.update');
Route::post('/products/destroy', [ProductsController::class, "destroy"])->middleware(['auth'])->name('products.destroy');


Route::resource('maps',MarkerController::class);
require __DIR__.'/auth.php';
